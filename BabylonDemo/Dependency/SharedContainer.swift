//
//  SharedContainer.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 20/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import Swinject

extension Container {
    // sharedContainer will provide the dependencies to other classes in the App.
    static let sharedContainer: Container = {
        let container = Container()
        container.register(PostServiceProtocol.self) { _ in PostService() }.inObjectScope(.container)
        container.register(DatabaseProtocol.self) { _ in Database() }.inObjectScope(.container)
        return container
    }()
}
