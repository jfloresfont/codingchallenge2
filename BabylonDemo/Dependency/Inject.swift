//
//  Inject.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 20/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import Swinject

func inject<T>(_ type: T.Type) -> T {
    return Container.sharedContainer.resolve(type)!
}
