//
//  Database.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 20/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import RxRealm
import RealmSwift
import RxSwift

protocol DatabaseProtocol {
    func storePosts(from observable: Observable<[Post]>) -> Disposable
    func storeComments(from observable: Observable<[Comment]>) -> Disposable
    func storeUser(from observable: Observable<User>) -> Disposable

    func getPosts() -> Observable<[Post]>
    func getComments(from postId: Int) -> Observable<[Comment]>
    func getUser(id: Int) -> Observable<User>
}

final class Database: DatabaseProtocol {

    private let realm = try! Realm()
    private let disposeBag = DisposeBag()

    func storePosts(from observable: Observable<[Post]>) -> Disposable {
        return storeItems(from: observable)
    }

    func storeComments(from observable: Observable<[Comment]>) -> Disposable {
        return storeItems(from: observable)
    }

    func storeUser(from observable: Observable<User>) -> Disposable {
        return storeItems(from: observable)
    }

    func getPosts() -> Observable<[Post]> {
        return Observable.array(from: realm.objects(Post.self))
    }

    func getComments(from postId: Int) -> Observable<[Comment]> {
        return Observable.array(from: realm.objects(Comment.self)).map { comments -> [Comment] in
            return comments.filter { $0.postId == postId }
        }
    }

    func getUser(id: Int) -> Observable<User> {
        return Observable.from(optional: realm.object(ofType: User.self, forPrimaryKey: id))
    }

    private func storeItems<T: Object>(from observable: Observable<[T]>) -> Disposable {
        return observable.subscribe(Realm.rx.add(configuration: Realm.Configuration.defaultConfiguration, update: true, onError: nil))
    }

    private func storeItems<T: Object>(from observable: Observable<T>) -> Disposable {
        return observable.subscribe(Realm.rx.add(configuration: Realm.Configuration.defaultConfiguration, update: true, onError: nil))
    }
}
