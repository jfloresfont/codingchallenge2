//
//  User.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 20/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import Foundation
import RealmSwift

final class User: Object, Decodable {
    @objc dynamic var id: Int = Int.random(in: 0 ..< Int.max)
    @objc dynamic var name: String = ""

    override static func primaryKey() -> String? {
        return "id"
    }

    convenience init(id: Int, name: String) {
        self.init()
        self.id = id
        self.name = name
    }
}
