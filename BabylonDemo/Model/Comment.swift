//
//  Comment.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 20/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import Foundation
import RealmSwift

final class Comment: Object, Decodable {
    @objc dynamic var id: Int = Int.random(in: 0 ..< Int.max)
    @objc dynamic var postId: Int = Int.random(in: 0 ..< Int.max)
    @objc dynamic var name: String = ""
    @objc dynamic var body: String = ""

    override static func primaryKey() -> String? {
        return "id"
    }
}
