//
//  AppFlow.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 18/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import UIKit
import RxFlow

final class AppFlow: Flow {

    var root: Presentable {
        return window
    }

    private let window: UIWindow

    init(withWindow window: UIWindow) {
        self.window = window
    }

    func navigate(to step: Step) -> NextFlowItems {
        guard let step = step as? AppStep else { return .none }
        switch step {
        case .posts:
            return navigationToPosts()
        case let .post(post):
            return navigationToPost(post: post)
        }
    }

    private func navigationToPosts() -> NextFlowItems {

        let postsViewController = PostsViewController(nibName: nil, bundle: nil)
        let viewModel = PostsViewModel(postService: inject(PostServiceProtocol.self),
                                       database: inject(DatabaseProtocol.self))
        postsViewController.viewModel = viewModel

        let navigationController = UINavigationController(rootViewController: postsViewController)
        navigationController.navigationBar.prefersLargeTitles = true
        window.rootViewController  = navigationController

        return .one(flowItem: NextFlowItem(nextPresentable: postsViewController, nextStepper: viewModel))
    }

    private func navigationToPost(post: Post) -> NextFlowItems {

        let postDetailsViewController = PostDetailsViewController(nibName: nil, bundle: nil)
        let viewModel = PostDetailsViewModel(post: post,
                                             postService: inject(PostServiceProtocol.self),
                                             database: inject(DatabaseProtocol.self))
        postDetailsViewController.viewModel = viewModel

        let navigationController = (window.rootViewController as? UINavigationController)
        navigationController?.pushViewController(postDetailsViewController, animated: true)

        return .none
    }
}

final class AppStepper: Stepper {
    init() {
        step.accept(AppStep.posts)
    }
}
