//
//  AppStep.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 20/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import RxFlow

enum AppStep: Step, Equatable {
    case posts
    case post(post: Post)
}
