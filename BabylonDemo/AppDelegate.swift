//
//  AppDelegate.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 20/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import UIKit
import RxFlow
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private let coordinator = Coordinator()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        guard let window = self.window else { return false }
        return startAppCoordinator(with: window)
    }

    private func startAppCoordinator(with window: UIWindow) -> Bool {
        coordinator.coordinate(flow: AppFlow(withWindow: window), withStepper: AppStepper())
        return true
    }
}
