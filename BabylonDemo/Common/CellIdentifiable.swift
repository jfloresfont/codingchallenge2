//
//  CellIdentifiable.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 21/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import Foundation

protocol CellIdentifiable {
    static var cellId: String { get }
}
