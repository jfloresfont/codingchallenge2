//
//  APIService.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 20/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import Moya

enum APIService {
    case posts
    case comments(postId: Int)
    case user(id: Int)
}

extension APIService: TargetType {

    var baseURL: URL {
        return URL(string: "http://jsonplaceholder.typicode.com")!
    }

    var method: Moya.Method {
        switch self {
        case .posts, .comments, .user:
            return .get
        }
    }

    var path: String {
        switch self {
        case .posts:
            return "/posts"
        case let .user(id):
            return "/users/\(id)"
        case .comments:
            return "/comments"
        }
    }

    var task: Task {
        switch self {
        case .posts, .user:
            return .requestPlain
        case let .comments(postId):
            return .requestParameters(parameters: ["postId": postId],
                                      encoding: URLEncoding.queryString)
        }
    }

    var sampleData: Data {
        return Data()
    }

    var headers: [String: String]? {
        return ["Accept": "application/json"]
    }
}
