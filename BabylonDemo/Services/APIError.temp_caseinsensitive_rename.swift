//
//  ApiError.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 19/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

struct APIError: Error, Decodable, Equatable {
    let message: String
    let error: Int

    static let generic = APIError(message: "something.went.wrong".localized, error: 0)
}
