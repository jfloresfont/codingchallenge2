//
//  PostService.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 20/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import RxSwift
import Result
import Moya

protocol PostServiceProtocol {
    func posts() -> Single<Result<[Post], APIError>>
    func comments(from postId: Int) -> Single<Result<[Comment], APIError>>
    func user(id: Int) -> Single<Result<User, APIError>>
}

struct PostService: PostServiceProtocol {

    private let provider: MoyaProvider<APIService>

    init(provider: MoyaProvider<APIService> = MoyaProvider<APIService>()) {
        self.provider = provider
    }

    func posts() -> Single<Result<[Post], APIError>> {
        return request(service: .posts)
    }

    func comments(from postId: Int) -> Single<Result<[Comment], APIError>> {
        return request(service: .comments(postId: postId))
    }

    func user(id: Int) -> Single<Result<User, APIError>> {
        return request(service: .user(id: id))
    }

    private func request<T: Decodable>(service: APIService) -> Single<Result<T, APIError>> {
        return provider.rx.request(service)
            .map { response -> Result<T, APIError> in
                switch response.statusCode {
                case 200...299:
                    return try .success(JSONDecoder().decode(T.self, from: response.data))
                default:
                    return .failure(APIError(message: response.description, error: response.statusCode))
                }
            }.catchError { error -> Single<Result<T, APIError>> in
                return .just(Result.failure(APIError(message: error.localizedDescription, error: 0)))
        }
    }
}
