//
//  PostsViewModel.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 20/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import RxSwift
import RxCocoa
import Result
import RxFlow

final class PostsViewModel: Stepper, ViewModelType {

    private let postService: PostServiceProtocol
    private let database: DatabaseProtocol

    init(postService: PostServiceProtocol, database: DatabaseProtocol) {
        self.postService = postService
        self.database = database
    }

    func transform(input: Input) -> Output {

        let postsResponse = input.pullToRefreshTriggered
            .debounce(1.0, scheduler: MainScheduler.instance)
            .startWith(())
            .flatMapLatest(postService.posts).share()

        let postsResponseWithSuccess = postsResponse
            .flatMap { result -> Observable<[Post]> in
                if case let .success(posts) = result { return .just(posts) }
                return .never()
            }

        let refreshing = Observable.merge(input.pullToRefreshTriggered.map { true },
                                          postsResponse.map { _ in false })
            .asDriver(onErrorJustReturn: false)
            .distinctUntilChanged()

        let error = postsResponse.map { result -> String in
            if case let .failure(error) = result { return error.message }
            return ""
        }.asDriver(onErrorJustReturn: "something.went.wrong".localized)
        .filter { !$0.isEmpty }

        let posts = Observable.merge(database.getPosts(), postsResponseWithSuccess).asDriver(onErrorJustReturn: [])

        let navigation = input.postSelected
            .do(onNext: { [weak self] post in
                guard let self = self else { return }
                self.step.accept(AppStep.post(post: post))
            }).subscribe()
        let storePosts = database.storePosts(from: postsResponseWithSuccess)

        return Output(refreshing: refreshing,
                      posts: posts,
                      error: error,
                      disposables: [navigation, storePosts])
    }

    struct Input {
        let pullToRefreshTriggered: Observable<Void>
        let postSelected: Observable<Post>
    }

    struct Output {
        let refreshing: Driver<Bool>
        let posts: Driver<[Post]>
        let error: Driver<String>
        let disposables: [Disposable]
    }
}
