//
//  PostsViewController.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 20/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit

final class PostsViewController: UIViewController {

    var viewModel: PostsViewModel!

    private let refreshControl = UIRefreshControl()

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.refreshControl = self.refreshControl
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60
        tableView.tableFooterView = UIView()
        tableView.register(PostCell.self, forCellReuseIdentifier: PostCell.cellId)
        return tableView
    }()

    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        setupUI()
        bindViewModel()
    }

    private func setupUI() {
        title = "posts".localized
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    private func bindViewModel() {
        let output = viewModel.transform(input: PostsViewModel.Input(pullToRefreshTriggered: refreshControl.rx.controlEvent(.valueChanged).asObservable(),
                                                                     postSelected: tableView.rx.modelSelected(Post.self).asObservable()))

        output.refreshing.drive(refreshControl.rx.isRefreshing).disposed(by: disposeBag)

        output.posts.drive(tableView.rx.items(cellIdentifier: PostCell.cellId, cellType: PostCell.self)) { _, model, cell in
            cell.textLabel?.text = model.title
        }.disposed(by: disposeBag)

        output.error.drive(onNext: { [weak self] text in
            guard let self = self else { return }
            self.presentAlertController(title: "", message: text)
        }).disposed(by: disposeBag)

        output.disposables.forEach { $0.disposed(by: disposeBag) }
    }
}
