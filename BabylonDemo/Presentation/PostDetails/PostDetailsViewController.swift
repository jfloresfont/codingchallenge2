//
//  PostViewController.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 20/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SnapKit
import RxDataSources

final class PostDetailsViewController: UIViewController {
    var viewModel: PostDetailsViewModel!

    private let disposeBag = DisposeBag()

    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 60
        tableView.tableFooterView = UIView()
        tableView.register(PostDetailsCell.self, forCellReuseIdentifier: PostDetailsCell.cellId)
        return tableView
    }()

    private lazy var activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .gray)
        activityIndicator.hidesWhenStopped = true
        return activityIndicator
    }()

    override func viewDidLoad() {
        setupUI()
        bindViewModel()
    }

    private func setupUI() {
        title = "details".localized
        view.backgroundColor = .white
        view.addSubview(tableView)
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }

        view.addSubview(activityIndicator)
        activityIndicator.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }

    private func bindViewModel() {
        let output = viewModel.transform(input: PostDetailsViewModel.Input())

        let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, PostDetailsViewModel.Item>>(configureCell: { dataSource, table, indexPath, item in
            switch item {
            case let .author(text), let .description(text), let .numComments(text):
                let cell = table.dequeueReusableCell(withIdentifier: PostDetailsCell.cellId)!
                cell.textLabel?.text = text
                return cell
            }
        })
        dataSource.titleForHeaderInSection = { dataSource, index in
            return dataSource.sectionModels[index].model
        }
        output.postDetails
            .drive(tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        output.error.drive(onNext: { [weak self] text in
            guard let self = self else { return }
            self.presentAlertController(title: "", message: text)
        }).disposed(by: disposeBag)

        output.refreshing.drive(tableView.rx.isHidden).disposed(by: disposeBag)
        output.refreshing.drive(activityIndicator.rx.isAnimating).disposed(by: disposeBag)

        output.disposables.forEach { $0.disposed(by: disposeBag) }
    }
}
