//
//  PostViewModel.swift
//  BabylonDemo
//
//  Created by Javier Flores Font on 20/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources
import Result

struct PostDetailsViewModel: ViewModelType {

    private let postService: PostServiceProtocol
    private let database: DatabaseProtocol
    private let post: Post

    init(post: Post, postService: PostServiceProtocol, database: DatabaseProtocol) {
        self.post = post
        self.postService = postService
        self.database = database
    }

    func transform(input: Input) -> Output {

        let commentsServerResponse = postService.comments(from: post.id).asObservable().share()
        let commentsServerResponseWithSuccess = commentsServerResponse.flatMap { result -> Observable<[Comment]> in
            if case let .success(comments) = result { return .just(comments) }
            return .never()
        }
        let userServerResponse = postService.user(id: post.userId).asObservable().share()
        let userServerResponseWithSuccess = userServerResponse.flatMap { result -> Observable<User> in
            if case let .success(user) = result { return .just(user) }
            return .never()
        }
        let postDetailsServerResponse = Observable.combineLatest(userServerResponse, commentsServerResponse)
        let postDetailsServerResponseWithSuccess = Observable.combineLatest(userServerResponseWithSuccess, commentsServerResponseWithSuccess)

        let databaseUser = database.getUser(id: post.userId)
        let databaseComments =  database.getComments(from: post.id)
        let databasePostDetails = Observable.combineLatest(databaseUser, databaseComments)

        // It shows the content from the database first, and later on we can receive an update from the server
        let postDetails = Observable.merge(databasePostDetails, postDetailsServerResponseWithSuccess)
            .map { details -> [SectionModel<String, Item>] in
                return [SectionModel(model: "body".localized, items: [Item.description(self.post.body)]),
                        SectionModel(model: "author".localized, items: [Item.description(details.0.name)]),
                        SectionModel(model: "number.of.comments".localized, items: [Item.description("\(details.1.count)")])]
            }.asDriver(onErrorJustReturn: [])

        // It shows just one error if both requests fails
        let error = postDetailsServerResponse
            .flatMap { commentResult, userResult -> Observable<String> in
                if case let Result.failure(error) = commentResult { return .just(error.message) }
                if case let Result.failure(error) = userResult { return .just(error.message) }
                return .never()
            }.asDriver(onErrorJustReturn: "something.went.wrong".localized)

        let refreshing =
            Observable.merge(postDetailsServerResponse.map { _ in false },
                             databasePostDetails.map { _ in false })
            .startWith(true)
            .asDriver(onErrorJustReturn: false)

        // We need to subscribe always from the UIViewController, not from the ViewModel, that's why we are exposing some disposables as an output.
        let disposables = [database.storeComments(from: commentsServerResponseWithSuccess),
                           database.storeUser(from: userServerResponseWithSuccess)]

        return Output(refreshing: refreshing,
                      postDetails: postDetails,
                      error: error,
                      disposables: disposables)
    }

    struct Input {}

    struct Output {
        let refreshing: Driver<Bool>
        let postDetails: Driver<[SectionModel<String, Item>]>
        let error: Driver<String>
        let disposables: [Disposable]
    }

    enum Item {
        case description(String)
        case author(String)
        case numComments(String)
    }
}
