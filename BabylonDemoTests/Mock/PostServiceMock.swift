//
//  PostServiceMock.swift
//  BabylonDemoTests
//
//  Created by Javier Flores Font on 22/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import Foundation
import RxSwift
import Result

@testable import BabylonDemo

final class PostServiceMock: PostServiceProtocol {

    var postsResult: BehaviorSubject<Result<[Post], APIError>> = BehaviorSubject(value: Result.success([]))
    var postsCalled: Bool = false
    func posts() -> Single<Result<[Post], APIError>> {
        postsCalled = true
        return postsResult.asSingle()
    }

    var commentsResult: BehaviorSubject<Result<[Comment], APIError>> = BehaviorSubject(value: Result.success([]))
    var commentsCalled: Bool = false
    func comments(from postId: Int) -> Single<Result<[Comment], APIError>> {
        commentsCalled = true
        return commentsResult.asSingle()
    }

    var userResult: BehaviorSubject<Result<User, APIError>> = BehaviorSubject(value: Result.success(User()))
    var userCalled: Bool = false
    func user(id: Int) -> Single<Result<User, APIError>> {
        userCalled = true
        return userResult.asSingle()
    }
}
