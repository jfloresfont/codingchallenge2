//
//  DatabaseMock.swift
//  BabylonDemoTests
//
//  Created by Javier Flores Font on 22/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa
import Result

@testable import BabylonDemo

final class DatabaseMock: DatabaseProtocol {

    private let posts = PublishSubject<[Post]>()
    func storePosts(from observable: Observable<[Post]>) -> Disposable {
        return observable.bind(to: posts)
    }

    private let comments = PublishSubject<[Comment]>()
    func storeComments(from observable: Observable<[Comment]>) -> Disposable {
        return observable.bind(to: comments)
    }

    private let user = PublishSubject<User>()
    func storeUser(from observable: Observable<User>) -> Disposable{
        return observable.bind(to: user)
    }

    func getPosts() -> Observable<[Post]> {
        return posts
    }

    func getComments(from postId: Int) -> Observable<[Comment]> {
        return comments
    }

    func getUser(id: Int) -> Observable<User> {
        return user
    }
}
