//
//  Extension+TestScheduler.swift
//  BabylonDemoTests
//
//  Created by Javier Flores Font on 23/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import RxSwift
import RxTest
import RxFlow

@testable import BabylonDemo

extension TestScheduler {
    /**
     Builds testable observer for s specific observable sequence, binds it's results and sets up disposal.

     - parameter source: Observable sequence to observe.
     - returns: Observer that records all events for observable sequence.
     */
    func record<O: ObservableConvertibleType>(source: O) -> TestableObserver<O.E> {
        let observer = createObserver(O.E.self)
        let disposable = source.asObservable().bind(to: observer)
        scheduleAt(100000) {
            disposable.dispose()
        }
        return observer
    }

    func record(stepper: Stepper) -> TestableObserver<AppStep>{
        return record(source: stepper.step.flatMap{step -> Observable<AppStep> in
            if let step = step as? AppStep {
                return .just(step)
            } else {
                return .empty()
            }
        })
    }
}
