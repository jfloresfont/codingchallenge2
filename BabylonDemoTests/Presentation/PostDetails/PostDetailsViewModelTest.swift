//
//  PostDetailsViewModelTest.swift
//  BabylonDemoTests
//
//  Created by Javier Flores Font on 23/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import XCTest
import RxTest
import RxBlocking
import RxSwift
import Quick
import Nimble

@testable import BabylonDemo

final class PostDetailsViewModelTest: QuickSpec {
    private var postServiceMock: PostServiceMock!
    private var databaseMock: DatabaseMock!
    private var viewModel: PostDetailsViewModel!
    private var output: PostDetailsViewModel.Output!
    private var disposeBag: DisposeBag!
    private var user: User!
    private var comment: Comment!
    private var post: Post!

     override func spec() {

        describe("When PostsViewModel is set up") {

            self.databaseMock = DatabaseMock()
            self.postServiceMock = PostServiceMock()
            self.post = Post()
            self.user = User()
            self.comment = Comment()
            self.viewModel = PostDetailsViewModel(post: self.post,
                                                  postService: self.postServiceMock,
                                                  database: self.databaseMock)

            self.output = self.viewModel.transform(input: PostDetailsViewModel.Input())
            self.disposeBag = DisposeBag()

            it("Should call comments endpoint") {
                expect(self.postServiceMock.commentsCalled).to(equal(true))
            }

            it("Should call user endpoint") {
                expect(self.postServiceMock.userCalled).to(equal(true))
            }

            describe("When comments service fails") {

                let testScheduler = TestScheduler(initialClock: 0)
                let result = testScheduler.record(source: output.error)

                testScheduler.scheduleAt(10) {
                    self.postServiceMock.commentsResult.onNext(.failure(APIError.generic))
                    self.postServiceMock.userResult.onNext(.success(self.user))
                }
                testScheduler.start()

                it("Should show error") {
                    expect(result.events.first?.value.element).toNot(beNil())
                }
            }

            describe("When user service fails") {

                let testScheduler = TestScheduler(initialClock: 0)
                let result = testScheduler.record(source: output.error)

                testScheduler.scheduleAt(10) {
                    self.postServiceMock.commentsResult.onNext(.success([self.comment]))
                    self.postServiceMock.userResult.onNext(.failure(APIError.generic))
                }
                testScheduler.start()

                it("Should show error") {
                    expect(result.events.first?.value.element).toNot(beNil())
                }
            }

            describe("When comments and user services fails") {
                let testScheduler = TestScheduler(initialClock: 0)
                let result = testScheduler.record(source: output.error)

                testScheduler.scheduleAt(10) {
                    self.postServiceMock.commentsResult.onNext(.failure(APIError.generic))
                    self.postServiceMock.userResult.onNext(.failure(APIError.generic))
                }
                testScheduler.start()

                it("Should show error") {
                    expect(result.events.first?.value.element).toNot(beNil())
                }
            }

            describe("When comments and user service success") {
                let testScheduler = TestScheduler(initialClock: 0)

                let result = testScheduler.record(source: output.error)

                testScheduler.scheduleAt(10) {
                    self.postServiceMock.commentsResult.onNext(.success([self.comment]))
                    self.postServiceMock.userResult.onNext(.success(self.user))
                }
                testScheduler.start()

                it("Should show error") {
                    expect(result.events.last?.value.element).to(beNil())
                }
            }
        }
    }
}
