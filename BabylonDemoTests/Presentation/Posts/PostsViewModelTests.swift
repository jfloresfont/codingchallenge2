//
//  PostsViewModelTests.swift
//  BabylonDemoTests
//
//  Created by Javier Flores Font on 22/04/2019.
//  Copyright © 2019 javierflores. All rights reserved.
//

import XCTest
import RxTest
import RxBlocking
import RxSwift
import Quick
import Nimble

@testable import BabylonDemo

final class PostsViewModelTests: QuickSpec {
    private var postServiceMock: PostServiceMock!
    private var databaseMock: DatabaseMock!
    private var viewModel: PostsViewModel!
    private var output: PostsViewModel.Output!
    private var pullToRefreshTriggered: PublishSubject<Void>!
    private var postSelected: PublishSubject<Post>!
    private var disposeBag: DisposeBag!

    override func spec() {

        describe("When PostsViewModel is set up") {

            self.pullToRefreshTriggered = PublishSubject<Void>()
            self.postSelected = PublishSubject<Post>()
            self.disposeBag = DisposeBag()
            self.databaseMock = DatabaseMock()
            self.postServiceMock = PostServiceMock()
            self.viewModel = PostsViewModel(postService: self.postServiceMock,
                                            database: self.databaseMock)
            self.output = self.viewModel.transform(input: PostsViewModel.Input(pullToRefreshTriggered: self.pullToRefreshTriggered,
                                                                               postSelected: self.postSelected))
            describe("When post selected") {

                let testScheduler = TestScheduler(initialClock: 0)

                let stepResult = testScheduler.record(stepper: self.viewModel)
                let postSelected = Post()
                testScheduler.scheduleAt(10) {
                    self.postSelected.onNext(postSelected)
                }
                testScheduler.start()

                it("Should navigate to post details") {
                    expect(stepResult.events.first?.value.element).toEventually(equal(AppStep.post(post: postSelected)))
                }
            }

            describe("When pullToRefreshTriggered") {
                self.pullToRefreshTriggered.onNext(())

                context("When post service fails") {

                    let testScheduler = TestScheduler(initialClock: 0)
                    let result = testScheduler.record(source: output.error)

                    testScheduler.scheduleAt(10) {
                        self.postServiceMock.postsResult.onNext(.failure(APIError.generic))
                    }
                    testScheduler.start()

                    it("Should show error") {
                        expect(result.events.first?.value.element).toNot(beNil())
                    }

                    it("Should call posts service") {
                        expect(self.postServiceMock.postsCalled).to(beTrue())
                    }
                }

                context("When post service success") {

                    let testScheduler = TestScheduler(initialClock: 0)
                    let result = testScheduler.record(source: output.posts)

                    testScheduler.scheduleAt(10) {
                        self.postServiceMock.postsResult.onNext(.success([Post()]))
                    }
                    testScheduler.start()

                    it("Should return posts") {
                        expect(result.events.first?.value.element).toNot(beNil())
                    }

                    it("Should call posts service") {
                        expect(self.postServiceMock.postsCalled).to(beTrue())
                    }
                }
            }
        }
    }
}
